#include "tar.hpp"

Samstar::TarEntry Samstar::getFileFromTar(const std::string& tarfile, const std::string& targetFilename)
{
	int tarsize = 0;
	TarEntry entry{"", 0, nullptr};
	unsigned char* buffer = Utils::readFileContents(tarfile, &tarsize);

	if(buffer == nullptr)
		return entry;

	entry = Utils::readFileFromTarBuffer(buffer, targetFilename, tarsize);

	delete[] buffer;

	return entry;
}

std::vector<Samstar::TarEntry> Samstar::getAllFilesFromTar(const std::string &tarfile)
{
	int tarsize = 0;
	std::vector<TarEntry> entries;
	unsigned char* buffer = Utils::readFileContents(tarfile, &tarsize);

	if(buffer == nullptr)
		return entries;

	entries = Utils::readAllFilesFromTarBuffer(buffer, tarsize);

	delete[] buffer;

	return entries;
}

std::vector<Samstar::TarEntry> Samstar::getFilesFromTarRegex(const std::string &tarfile, const std::regex& pattern)
{
	// @TODO: We could of course do the regex match when going through the
	//        tar entries, but this way I don't have to write that much new code.
	//        Also, I personally accept the performance loss

	std::vector<TarEntry> entries;
	std::vector<TarEntry> allFiles = getAllFilesFromTar(tarfile);

	for(auto& entry : allFiles)
	{
		if(std::regex_match(entry.filename, pattern))
		{
			entries.push_back(entry);
		}
	}

	return entries;
}

int Samstar::Utils::octalToInt(int octal)
{
	char buffer[MAX_FILE_SIZE+1];
	char* index;
	int multiplier = 0, decimal = 0;

	snprintf(buffer, sizeof(buffer), "%d", octal);
	index = &buffer[strlen(buffer)-1];

	do
	{
		int digit = *index-'0';
		decimal += digit*pow(8, multiplier++);
	}while(*--index);

	return decimal;
}

unsigned char* Samstar::Utils::readFileContents(const std::string& filename, int* size)
{
	int fd = open(filename.c_str(), O_RDONLY);
	int tarsize = 0;
	unsigned char* buffer = nullptr;

	if(fd < 0)
		goto end;

	tarsize = lseek(fd, 0, SEEK_END);

	buffer = new unsigned char[tarsize];

	lseek(fd, 0, SEEK_SET);
	read(fd, buffer, tarsize);

	close(fd);

end:
	if(size != nullptr)
		*size = tarsize;

	return buffer;
}

Samstar::TarEntry Samstar::Utils::readFileFromTarBuffer(const unsigned char* buffer, const std::string& targetFilename, int tarsize)
{
	using namespace Samstar;

	int offset = 0;
	TarEntry targetFile{"", 0, nullptr};

	while(offset < tarsize)
	{
		TarEntry entry{"", 0, nullptr};
		char filenameBuffer[MAX_FILE_NAME];
		char filesizeBuffer[MAX_FILE_SIZE];

		snprintf(filenameBuffer, MAX_FILE_NAME, "%s", buffer+offset);
		entry.filename = filenameBuffer;

		if(entry.filename == "")
			break;

		snprintf(filesizeBuffer, MAX_FILE_SIZE, "%s", buffer+offset+FILE_SIZE_OFFSET);
		entry.filesize = Utils::octalToInt(atoi(filesizeBuffer));

		if(entry.filename == targetFilename)
		{
			entry.data = new unsigned char[entry.filesize];
			memcpy(entry.data, buffer+offset+FILE_CONTENT_OFFSET, entry.filesize);
			targetFile = entry;

			break;
		}

		offset += offsetToNextEntry(entry.filesize);
	}

	return targetFile;
}

std::vector<Samstar::TarEntry> Samstar::Utils::readAllFilesFromTarBuffer(const unsigned char* buffer, int tarsize)
{
	using namespace Samstar;

	std::vector<TarEntry> entries;
	int offset = 0;

	while(offset < tarsize)
	{
		TarEntry entry{"", 0, nullptr};
		char filenameBuffer[MAX_FILE_NAME];
		char filesizeBuffer[MAX_FILE_SIZE];

		snprintf(filenameBuffer, MAX_FILE_NAME, "%s", buffer+offset);
		entry.filename = filenameBuffer;

		if(entry.filename == "")
			break;

		snprintf(filesizeBuffer, MAX_FILE_SIZE, "%s", buffer+offset+FILE_SIZE_OFFSET);
		entry.filesize = Utils::octalToInt(atoi(filesizeBuffer));

		entry.data = new unsigned char[entry.filesize];
		memcpy(entry.data, buffer+offset+FILE_CONTENT_OFFSET, entry.filesize);

		entries.push_back(entry);

		offset += offsetToNextEntry(entry.filesize);
	}

	return entries;
}

int Samstar::Utils::offsetToNextEntry(int filesize)
{
	int offset = 0;
	int nblocks = filesize / BLOCK_SIZE;
	int modulo = filesize % BLOCK_SIZE;

	offset += BLOCK_SIZE; // Header
	offset += BLOCK_SIZE * nblocks; // Content
	offset += modulo ? BLOCK_SIZE : 0; // Rounding up

	return offset;
}
