#pragma once

#define MAX_FILE_NAME 100
#define MAX_FILE_SIZE 12
#define BLOCK_SIZE 512
#define FILE_SIZE_OFFSET 124
#define FILE_CONTENT_OFFSET 512

#include <iostream>
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
#include <regex>

namespace Samstar{

	typedef struct {
		std::string filename;
		int filesize;
		unsigned char* data;
	}TarEntry;

	TarEntry getFileFromTar(const std::string& tarfile, const std::string& targetFilename);
	std::vector<TarEntry> getAllFilesFromTar(const std::string &tarfile);
	std::vector<TarEntry> getFilesFromTarRegex(const std::string &tarfile, const std::regex& pattern);

namespace Utils{
	int octalToInt(int octal);
	unsigned char* readFileContents(const std::string& filename, int* size=nullptr);
	TarEntry readFileFromTarBuffer(const unsigned char* buffer, const std::string& targetFilename, int tarsize);
	std::vector<TarEntry> readAllFilesFromTarBuffer(const unsigned char* buffer, int tarsize);
	int offsetToNextEntry(int filesize);
}
}
