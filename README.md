### Samstar the simple tar library

Very simple C++ tar library that can fetch all files or a specific file from a tar archive. Does not contain much error checking or support for symbolic links.
